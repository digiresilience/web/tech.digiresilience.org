---
layout: page
title: About
permalink: /about/
---

[Center for Digital Resilience](https://digiresilience.org) builds resilient systems to keep civil society online. We believe that digital security is a collective effort. We work with partners to bolster the digital strength and wellness of every individual within a community, and we aim to not only ensure timely and effective responses for our communities, but to reduce their need for emergency response in the first place.

We operate as a network of communities, working hand-in-hand with trusted local partners. Through our communities, we provide a sustainable, strategic approach to long-term digital wellness.

We work with a variety of organizations, including funders protecting their grantees; trusted local leaders; and technical support providers.

Much of this blog covers the development of CDR Link, our ticketing platform and threat data ecosystem. You can read [this post](https://tech.digiresilience.org/2019/05/30/what-is-CDR-link.html) to learn more about Link. 

Go [here](https://digiresilience.org/about) to learn more, and find us on [GitLab](https://gitlab.com/digiresilience) for documentation and code.
