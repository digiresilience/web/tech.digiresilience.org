---
layout: post
title:  "Leafcutter: The glue binding the data together"
date:   2019-06-03 09:45
author: by Josh
---

[CDR Link](/2019/05/30/what-is-CDR-link.html) isn't a single platform, it's an ecoystem of interconnected technologies and data streams. Users file tickets in Zammad via extensions integrating email, web forms, Signal, WhatsApp, and Telegram. That data will be scrubbed and normalized and sent to MISP, the open-source threat sharing and analysis database. Third-party data from trusted parters will combine with ticketing data in MISP. All datasets will be made accessible for analysis and sharing with the non-technical community via a data usability and accessibility layer.

Our name for the glue joining together all of these pieces is "Leafcutter."

<!--more-->

Why "Leafcutter"? It's named after the ant. From Wikipedia:

> Leafcutter ants can carry twenty times their body weight and cut and process fresh vegetation (leaves, flowers, and grasses) to serve as the nutritional substrate for their fungal cultivates... The ants actively cultivate their fungus, feeding it with freshly cut plant material and keeping it free from pests and molds.

Millions of leafcutter ants work separately, but side-by-side, on simple, discrete tasks that contribute to the cultivation of fungus that serves as the entire colony's food. Life in a leafcutter colony is like a giant open source software project.

## Wy are we building this?

Our goal is to pull in multiple sources of data -- from our own ticketing platform, from trusted partners' ticketing platforms, from friends conducting network analysis, and more -- to make it more accessible and actionable for CDR communities and others within our network of human rights technology and security providers.

The latter need more data to understand the digital threats others in their communities are facing. This includes trends and patterns, best practices for mitigation, regional focus and global reach, adversarial data, and integration of data on common low-level attacks (phishing attempts, malware attachments) with data on high-level cyberattacks (corporate breaches, social engineering attacks).

These needs can be met in party through the submission of datasets from across civil society and the corporate sector, enabling greater intelligence in the ongoing fight to protect the most vulnerable from digital attack.

## How will we do it?

Leafcutter will utilize open platforms and standards. The core parts will include:

- The [Zammad](https://zammad.org/) open source ticketing platform, with customized defaults to increase security
- [Sigarillo](https://gitlab.com/digiresilience/link/sigarillo) and [QuePasa](https://gitlab.com/digiresilience/link/quepasa), new Zammmad extensions for Signal and WhatsApp respectively (allowing secure ticket creation over those platforms)
- [MISP](https://www.misp-project.org/), the open source threat intelligence sharing platform
- Third-party datasets detailing malware attacks, network outages, and other threats
- APIs to send data *to* MISP and to display that data *outside* of MISP  
- Mechanisms to display and analyze MISP data

## Data

Data should be based on the needs of the members and would include information on threats, vulnerabilities, risks, incidents, and tactics, techniques, and procedures used by threat actors. Types of data could also be actual indicators of compromise (IOCs) including IP addresses, hashes, and command and control information.

Data may be sourced from a variety of communities such as other human rights and technology projects, partner organizations and communities, vendors, professional organizations, and directly from CDR members. The traffic light protocol (https://www.us-cert.gov/tlp) will be used to indicate how information may be further disseminated and used in the community. Furthermore, attribution may be included in some datasets while anonymous in others. Providing source attribution, where possible, can help enable collaboration and trust, and may increase the usability of the information.

## What's next?

We're already running a hardened version of Zammad, along with security extensions, for two CDR communities. We're now sketching out our approach to share Zammad ticketing data and third-party datasets with our MISP instance, and how we will make it easy to draw that data out and make it actionable. We'll share updates here as our progress continues. 
