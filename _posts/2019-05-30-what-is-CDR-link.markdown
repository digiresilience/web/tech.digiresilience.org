---
layout: post
title:  "What is CDR Link?"
date:   2019-05-30 09:45
author: by Josh 
---

Our technology work at the Center for Digital Resilience centers on development of the "CDR Link" project. Link is an ecosystem comprised of a number of open source tools that address number of current challenges facing the civil society incident responder community.
<!--more-->

These tools include:

* **Zammad help desk**: A customized version of Zammad, an open source, flexible, multi-channel help desk platform that enables those in need to ask for help, and providers to respond quickly

* **Custom settings**: CDR Link adjusts Zammad’s defaults to ensure a secure hosting setup and adapt the platform to the needs of civil society organizations and human rights incident responders

* **Multi-channel integrations**: Integrations with popular messaging apps, including WhatsApp, Telegram, Signal, and Twitter

* **Security**: Secure methods of ticket creation and communication that don't require users to install complex and difficult software like PGP

* **Leafcutter**: A custom federated threat analysis and data accessibility tool that incorporates datasets from community partners and integrates them with the rest of the Link stack, enabling the development and analysis of a community-wide dataset of threat information
*In development*

* **MISP**: Open source platform enabling information sharing of threat intelligence including cyber security indicators
*In development*

* **Documentation**: Detailed, plain-text documentation of every inch of the software stack, including support for self hosting, resources for digital security trainers, and resources from the wider community

CDR Link is guided by the community-minded, open source, sustainability-focused ethic of our programmatic work. These principles include:

* **Security**: Security and privacy are our first priority, and must be included from the start, never an afterthought or add-on.

* **Interoperability**: Link’s technology, taxonomy, processes, and frameworks must interoperate with other platforms in our space.

* **Open source**: All technology that we create or adapt must be open source, ideally licensed under the GPL

* **Unix philosophy**: Many small parts doing single things well, rather than one big thing doing many things badly.

* **Part of the ecosystem**: All work must be done to contribute to and support the existing ecosystem of technology-supported efforts to defend human rights and protect civic engagement

* **Documentation**: Every technology and process must be documented in a way that makes third-party use as easy as possible

* **Packaging**: All component parts of the CDR Link stack will be packaged in a way that makes third-party deployment as easy as possible.

Look here for updates about the development of all of these pieces. Updates and details about our work to create Signal and WhatsApp extensions for Zammad -- enabling secure, end-to-end encrypted, mobile-friendly ticket creation -- will follow soon.  
