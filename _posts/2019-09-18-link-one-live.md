---
layout: post
title:  "CDR Link 1.0 is released!"
date:   2019-09-18 07:45
author: by Josh 
---

CDR Link has reached 1.0 status! 

Wait - what *is* CDR Link? 

The CDR Link platform is for anyone wishing to offer their community a secure, multi-channel, mobile-first way to get in touch and ask for assistance. It enables an easy way for communities to ask for help and for responder to provide it. It's super flexible and we're excited to hear how people will be using it. 

This release is the culmination of about two years' of work from concept to development to beta release to, finally, the 1.0. The CDR team -- including the development team at [Guardian Project](https://guardianproject.info) -- is excited to debut it! 

<!--more-->

## Who is this actually for? 

First and foremost, we developed CDR Link to address the needs of responders working with CDR's present and future communities, which are currently the [Digital Security Exchange](https://dsx.us) (DSX) in North America and some diaspora and in-country organizations in a MENA-region country. In the case of the DSX, we maintain a single CDR Link instance that is available to trusted partners to use to triage incoming requests for help and to assist individual organizations that request needs assessments, trainings, or other interventions. Our goal is for Link to be totally invisible to end users -- that is, the individuals or organizations served by DSX and other partners -- and, instead, for them to experience seamless, secure communications with trusted providers over the messaging platforms of their choice. Link should only be visible to those using it to respond to incoming requests for help. 

We are also in conversation with organizations around the world about setting up their own instances of Link, to be used separately from CDR's existing implementations. If you would like to discuss how Link might be right for you, or if you are simply interested in learning more, contact me at [josh@digiresilience.org](mailto:josh@digiresilience.org). 

## What is CDR Link? 
What are we actually talking about when we say "CDR Link"? As we described [here](https://tech.digiresilience.org/2019/05/30/what-is-CDR-link.html), CDR Link is a helpdesk platform based on [Zammad](https://zammad.org), with custom defaults and additional messaging plugins that enable human rights responders to safely and efficiently respond to organizations and actors seeking assistance. 

Link's components include: 

* **Zammad helpdesk**: A customized and hardened version of Zammad, an open source, flexible, multi-channel ticketing platform that enables those in need to ask for help, and providers to respond.

* **Secure messaging channels**: Integrations with popular messaging apps, including WhatsApp, Telegram, Signal, and Twitter, making it easy to securely reach responders to request assistance. 

* **Custom settings**: CDR Link adjusts Zammad’s defaults to ensure a secure hosting setup.

* **Security**: Secure methods of ticket creation and communication that don’t require users to install complex and difficult software like PGP.

Below is a breakdown of each feature. 

## Zammad 

The [Zammad](https://zammad.org/) customer service ticketing platform forms the core of the Link universe. It's a robust open-source project that enables "agents" (for us, that means responders) to connect to "customers" (organizations or individuals in need of assistance). Zammad boasts a strong and engaged community of contributors and is *very* flexible and extensible. We adopted it because it's a well-built and well-documented platform that allows -- and encourages -- people to customize it to suit their needs. Our adaptation of Zammad is available [here](https://gitlab.com/digiresilience/link/zammad). 

From the perspective of a user in need of assistance, Zammad doesn't look like anything at all -- users don't see it and only interact with it via the messaging app of their choice. But when they ask for help by sending a message to a dedicated Signal or WhatsApp number, Telegram bot, email address, or when all else fails, SMS number, that message is received by Zammad and converted into a ticket. In turn, responders can respond on the messaging platform preferred by the user. When that platform is Signal and WhatsApp, users can be assured that their messages are end-to-end encrypted.

![WhatsApp ticket](/assets/images/whatsapp.png "WhatsApp ticket")
**Tickets are received and send using the same messaging platforms, like WhatsApp.** 

![Signal ticket](/assets/images/signal.png "Signal ticket")
**Option to reply using Signal**

## Channels 

Out of the box, Zammad features a number of messaging integrations that make it instantly usable as a helpdesk platform. These include the ability to receive tickets via Twitter, SMS, online forms, Telegram, and Facebook pages. None of these integrations provide the level of security that we need to guarantee our partners and members of our communities, so we've added new channels that add much more security to the platform. 

Signal and WhatsApp are two of the most-used messaging apps across CDR communities. So we've (and by we, I mean the amazing team at [Guardian Project](https://guardianproject.info) built and added two new extensions, [Signal bridge](https://gitlab.com/digiresilience/link/sigarillo) and [WhatsApp bridge](https://gitlab.com/digiresilience/link/quepasa), and the end-to-end encryption they use to the Zammad platform. The Signal bridge makes use of [Josh King's](https://github.com/throneless-tech/libsignal-service-javascript) excellent libsignal-service-javascript library (and of course, builds on the excellent foundation set by [Open Whisper Systems](https://signal.org/) and allows administrators to easily create new Signal accounts via a website, with no need for a phone (they can of course use an existing phone number, or grab a new number from Google Voice, Twilio, or another service). The WhatsApp functionally is built off the Signal bridge -- a crucial feature requested by existing CDR communities and many potential partners. 

Last but not least is the [Zammad-Twilio Recorder](https://gitlab.com/digiresilience/link/zammad-twilio-recorder), a simple extension that receives a call from a Twilio number, records it, and then uploads the audio file (in the form of an MP3) to Zammad as a ticket. This extension is especially useful in situations in which part or all of the internet is down in a particular region, creating the need to fallback to less-secure, non-internet-reliant methods of ticket creation like SMS and voicemail. 

## Custom settings

We have adjusted many of the Zammad defaults to increase user security, and add a few customizations of our own to do the same. Examples include: 

* **Removing the content of email notifications.** By default, new tickets prompt Zammad to send email notifications to "agents" (responders, in our lingo) that include the full content of the ticket. We've customized the platform to remove the content, and to only alert the agent of the existence of a new ticket. This retains the integrity of encrypted messages submitted via Signal or WhatsApp. 

* **Disabling external services lookup.** Zammad by default uses the services hosted at geo.zammad.org and images.zammad.org, as well as the Google Maps location service, for IP geolocation and user avatar lookup. This is a privacy leak, as it exposes user identities to third-party services. 

* **Hardened server setup.** We've developed the infrastructure to run all the various Link components securely on a server using all the modern hardening best practices: firewall, monitoring, automated infrastructure deploys. This deployment stack is also open source available here LINK. We host all of our infrastructure on servers run by [Greenhost](https://greenhost.nl), a long-time and trusted service provider operating in the human rights and technology space. 


## Security 

You may have noticed that support for PGP encrypted email is missing from the list above. This is intentional. While a number of similar systems do incorporate PGP in order to ensure the security of email-generated helpdesk requests, members of CDR communities are overwhelmingly mobile-first in their use of the internet and networked devices, and require the most immediate, "just works" method of submitting ticket requests. For this reason -- and also because of the general difficulty of using PGP-encrypted email for even the most technical members of our community --We have chosen to dedicate our resources to integrating the most commonly-used secure mobile platforms like Signal or WhatsApp. While no platform or protocol is perfect, we believe our choices are a good balance of centralization vs. decentralization, corporate-owned platforms vs. open-source and community-driven projects, and anonymity vs. identity-based platforms. 


## Learn more 

Want to dig into more details about CDR Link? Check the [CDR Link Guide](https://gitlab.com/digiresilience/link/link-documentation/blob/master/link-demo-info.md), which includes info about accessing a demo of the platform. 




























