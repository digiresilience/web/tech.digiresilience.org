---
layout: post
title:  "An ecosystem of extensions"
date:   2019-06-26 09:45
author: by Josh

---

As we've [mentioned before](https://tech.digiresilience.org/2019/05/30/what-is-CDR-link.html), CDR Link adheres to the [Unix-like philosophy](https://en.wikipedia.org/wiki/Unix_philosophy) of utilizing an ecosystem of small, repurpose-able programs that handle discreet tasks well, rather than producing one monolithic thing that does many things badly.

In software, the gold standard of this philosophy is the [Debian project](https://www.debian.org), in which thousands of volunteers maintain small programs that, in the aggregate, combine to form the Debian (GNU-Linux) operating system. If these volunteers worked to produce a single "blob" of a operating system, like macOS or Windows, that system would be less stable, less secure, and less flexible.

CDR Link isn't Debian, but we strive to apply the same philosophy. <!--more--> Rather than attempting to build a single platform that does everything we could possibly want (secure ticketing, constituent relations management, end-to-end encryption, malware analysis), we're focusing on extensability. For example, [Zammad](https://zammad.org), our core ticketing platform, is an open-source project developed by a robust community of staff and volunteers. It's already incredibly extensible, both in what it offers and the ways that outside platforms and projects can plug in. While we would like some of our work -- namely, our Signal and WhatsApp extensions -- to be merged upstream into the main Zammad codebase, we also recognize that other parts of the CDR Link ecosystem should exist separately from core platforms like Zammad and [MISP](https://www.misp-project.org).

This is where the "[Leafcutter](https://tech.digiresilience.org/2019/06/03/leafcutter.html)" project comes in. As we continue to extend Zammad with more secure points of entry, we also need to build pipelines that move data in and out of tickets so that it can be better analyzed, combined with other data sets, and shared within a trusted community. These pipes *could* be part of Zammad proper, or MISP proper, but we think the value is in building them as small, standalone programs that help integrate Link's constituent parts.

Enough with the concept. Here's what we've developed thus far:

- **Sigarillo**, to enable end-to-end encrypted ticket creation and communications via Signal
- **Grabadora**, to enable voicemail recordings (over regular voice connections) and, eventually, voicemail transcriptions and translations
- **QuePasa**, to enable end-to-end encrypted ticket creation and communications via Signal (coming soon)


Meanwhile, on the Leafcutter front, we'll be developing extensions that:

- Submit Zammad ticket data into MISP
- Integrate into MISP third-party datasets detailing malware attacks, network outages, and other threats
- A "usability layer" that makes it easier for users to view, submit, and analyze MISP data


If you have an idea for ways to extend the CDR Link ecosystem get in touch at [josh@digiresilience.org](mailto:josh@digiresilience.org).
